## Action Camera Swap Changelog

#### Version 1.9

* Actually packaged localizations
* Added SV localization (thanks Lysol!)

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/21464273) | [Nexus](https://www.nexusmods.com/morrowind/mods/51545)

#### Version 1.8

* Handled a couple more situations that were previously unhandled
* Various code optimizations and cleanups

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/19598337) | [Nexus](https://www.nexusmods.com/morrowind/mods/51545)

#### Version 1.7

* Fixed a bug that totally broke swapping
* Fixed another bug that prevented swapping when spawned in an exterior

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/19448707) | [Nexus](https://www.nexusmods.com/morrowind/mods/51545)

#### Version 1.6

* **[0.49+ only]** Automatic support for [Morrowind Interiors Project](https://www.nexusmods.com/morrowind/mods/52237) - interiors from this mod are properly seen as interiors despite being marked as "Quasi Exterior"
* Support the 0.49+ API for detecting a "quasi exterior"
* Removed the interface function since it was redundant, left the interface there so other mods can use it to detect Action Camera Swap if they want to
* Added DE localization

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/19447254) | [Nexus](https://www.nexusmods.com/morrowind/mods/51545)

#### Version 1.5

* Support quasi exteriors (e.g. `Mournhold, Great Bazaar`)
* Fix detecting indoors when swapping has been toggled off
* Now available on Nexus mods

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/7663326) | [Nexus](https://www.nexusmods.com/morrowind/mods/51545)

#### Version 1.4

* Slightly shortened the delay when swapping to third person to avoid some visual "snapping"
* Added a feature to auto-swap to first person when indoors (returning to third when outdoors; this is on by default)

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/7081146)

#### Version 1.3

* Added a settings menu; players can now disable the mod from the Options menu.
* Manually changing POV now disables automatic camera swapping (if it isn't already disable via Options)
* Added a script interface; other mods can detect if auto swapping is enabled or not
* Added documentation for the interface to the website

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/6910241)

#### Version 1.2

* Updated the mod to use the new types API (MR #1 by Tauin)

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/6100402)

#### Version 1.1

Added support for when a spell or weapon is readied via quick key events.

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/5585970)

#### Version 1.0

Initial version of the mod, supports rudimentary swapping.

[Download Link](https://gitlab.com/modding-openmw/action-camera-swap/-/packages/5464802)
